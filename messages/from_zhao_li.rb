require "test/unit"

class FromZhaoLi
  def message()
    return "testing: 1,2,3"
  end
end

class TestFromZhaoLi < Test::Unit::TestCase
  def test_message
    assert_equal("testing: 1,2,3", FromZhaoLi.new().message() )
  end
end

