FROM thomsch98/markserv as development
ARG APP_DIR=/usr/src/app/
WORKDIR ${APP_DIR}/

COPY . ${APP_DIR}


FROM development as builder
RUN scripts/build_app.sh


FROM registry.access.redhat.com/ubi8/ubi-minimal as production
ARG APP_DIR=/usr/src/app/
WORKDIR ${APP_DIR}/

COPY . ${APP_DIR}

