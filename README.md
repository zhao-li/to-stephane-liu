# To Stephane Liu
A repositories of messages for Stephane Liu

[![pipeline status](https://gitlab.com/zhao-li/to-stephane-liu/badges/master/pipeline.svg)](https://gitlab.com/zhao-li/to-stephane-liu/-/commits/master)
[![coverage report](https://gitlab.com/zhao-li/to-stephane-liu/badges/master/coverage.svg)](https://gitlab.com/zhao-li/to-stephane-liu/-/commits/master)

Prerequisites
-------------
1. install docker
1. install docker-compose
1. install git
1. clone repository: `git clone --recursive https://gitlab.com/zhao-li/to-stephane-liu.git`

Getting Started
---------------
1. run bootstrap.sh: `./bootstrap.sh`
1. start service: `docker-compose up`
1. for markdown, browse to http://localhost:3080

Add a message of your preferred format to the `./messages/` folder.

Compiling AsciiDoc
------------------
To compile AsciiDoc:

    $ docker-compose run asciidoc bash
    asciidoc$ scripts/compile_asciidoc.sh

Running Unit Tests
------------------
To run unit tests:

    $ docker-compose run unit-test bash
    unit-test$ scripts/run_tests.sh

Troubleshooting Pipeline Markdown
---------------------------------
To run the container used on the pipeline locally:

    $ docker-compose run --entrypoint "" pandoc /bin/sh
    pandoc$

To compile the Markdown files:

    pandoc$ scripts/compile_markdown.sh

