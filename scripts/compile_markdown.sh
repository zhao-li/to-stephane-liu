#!/bin/sh

# This script compiles the markdown files

for file in messages/*.md; do
  pandoc ${file} -f markdown -t html -s -o outputs/"$(basename $file)".html 
done

